# Changesets

We try hard to keep our users up to date about the changes that we make to our software. It is important that we keep track of all of these changes in a changelog. To make this an easy process, we use [Changesets](https://github.com/changesets/changesets/tree/main/docs). Please make sure that every Merge Request that you create has a changeset attached:

```shell
npm run add-changeset
```

You will be asked for a _bump type_ (patch, minor or major) and to summarize the changes you've made. Once you've added a changeset, a file will be added to the `.changeset` directory. You have to add this file to your commit.

If you make a super-tiny change (like fixing a typo or some other non-noteworthy change) that doesn't really require a changeset, you can add an empty changeset:

```shell
npm run add-changeset -- --empty
```

This will still add a changeset, but it will be empty. You will still need to commit the file.

Once we release a new version of the application, we can bundle all of the changesets for that version in a changelog:

```shell
npm run version-changeset
```

This will bundle all of the changesets for the new version and prepend it to the [Changelog](./CHANGELOG.md). You can then review the version changes to all the packages and make changes to the changelog if you wish. Once you are confident that the new changelog is correct, you can commit it to the repository, and a new version will be released.

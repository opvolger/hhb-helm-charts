{{/*
Generate DB connection URLs per service based on provided values or defaults.
*/}}

{{/*
Check service specific value first, then general value. If both are empty, then use database service full name.
If all of these are empty, this means the user did not configure a DB host but also did not enable the internal DB.
*/}}
{{- define "db.host" -}}
{{- coalesce .Values.database.host .Values.global.database.host (printf "%s-%s" $.Release.Name "database") -}}
{{- end -}}

{{/*
Check service specific value first, then general value. If both are empty, assume 5432. This would also be the one used
by the internal DB.
*/}}
{{- define "db.port" -}}
{{- coalesce .Values.database.port .Values.global.database.port "5432" -}}
{{- end -}}

{{/*
Check service specific value first, then general value. If both are empty, use chart name of service as DB username.
*/}}
{{- define "db.username" -}}
{{- coalesce .Values.database.username .Values.global.database.username .Chart.Name -}}
{{- end -}}

{{/*
Check service specific value first, then general value.
*/}}
{{- define "db.password" -}}
{{- coalesce .Values.database.password .Values.global.database.password -}}
{{- end -}}

{{/*
Define DB name based on service specific value. If empty, use name of service as default.
*/}}
{{- define "db.name" -}}
{{- coalesce .Values.database.name .Chart.Name -}}
{{- end -}}

{{/*
Generate database connection URL based on host, port, username, password and DB name.
*/}}
{{- define "db.url" -}}
{{- printf "postgresql://%s:%s@%s:%s/%s" (include "db.username" .) (include "db.password" .) (include "db.host" .) (include "db.port" .) (include "db.name" .) -}}
{{- end -}}

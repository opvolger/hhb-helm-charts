---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "postadressenservice.fullname" . }}
  labels:
    {{- include "postadressenservice.labels" . | nindent 4 }}
  annotations:
    checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "postadressenservice.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "postadressenservice.selectorLabels" . | nindent 8 }}
    spec:
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- if .Values.global.database.enabled }}
      initContainers:
        - name: {{ .Chart.Name }}-wait-for-db
          image: busybox
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_PORT
          command:
            - "sh"
            - "-c"
            - "while true; do sleep 1; echo Waiting for database...; if nc -z $POSTGRESQL_HOSTNAME $POSTGRESQL_PORT; then echo Database is up!; exit 0; fi; done; echo Aint gonna wait for the database forever...; exit 1"
        - name: {{ .Chart.Name }}-prepare-db
          image: bitnami/postgresql:13
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: POSTGRESQL_HOSTNAME
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_HOST
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_PORT
            - name: POSTGRESQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-%s-database" .Release.Name "database" }}
                  key: INTERNAL_DB_PASSWORD
            - name: POSTGRESQL_USERNAME_PADSVC
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_USERNAME
            - name: POSTGRESQL_PASSWORD_PADSVC
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_PASSWORD
            - name: POSTGRESQL_DATABASE_NAME_PADSVC
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_DATABASE_NAME
          command: [ /bin/sh ]
          args:
            - -c
            - >-
              export PGPASSWORD=$POSTGRESQL_PASSWORD &&
              export PGPORT=$POSTGRESQL_PORT &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE USER $POSTGRESQL_USERNAME_PADSVC WITH PASSWORD '$POSTGRESQL_PASSWORD_PADSVC'" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "CREATE DATABASE $POSTGRESQL_DATABASE_NAME_PADSVC" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $POSTGRESQL_DATABASE_NAME_PADSVC TO $POSTGRESQL_USERNAME_PADSVC" || true &&
              psql -h $POSTGRESQL_HOSTNAME -U postgres -c "ALTER USER $POSTGRESQL_USERNAME_PADSVC WITH PASSWORD '$POSTGRESQL_PASSWORD_PADSVC'" || true;
        - name: {{ .Chart.Name }}-migrate-db
          image: {{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: [ npm ]
          args: [ run, db:push ]
          env:
            - name: HHB_SECRET
              valueFrom:
                secretKeyRef:
                  name: {{ printf "%s-%s" .Release.Name "huishoudboekje" }}-jwt
                  key: key
            - name: DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_URL
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          {{- if eq .Values.global.platform "ocp3" }}
          livenessProbe:
            httpGet:
              path: /health
              port: http
            initialDelaySeconds: 60
          readinessProbe:
            httpGet:
              path: /health
              port: http
            initialDelaySeconds: 60
          {{- else }}
          startupProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
            failureThreshold: 60
          livenessProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health
              port: http
            periodSeconds: 10
        {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          env:
            - name: DATABASE_URL
              valueFrom:
                secretKeyRef:
                  name: {{ include "postadressenservice.fullname" . }}-database
                  key: POSTGRESQL_URL

{{/*
When no Keycloak admin username is provided, use admin as default.
*/}}
{{- define "keycloak.auth.username" }}{{ coalesce .Values.authentication.username "admin" }}{{- end }}

{{/*
If no Keycloak admin password is provided, generate it using 16 characters 0-9, a-z, A-Z.

A provided Keycloak admin password and the generated password are saved in different templates so that in NOTES.txt it
is possible to determine which one was used.
*/}}
{{- define "keycloak.auth.password" }}{{- .Values.authentication.password }}{{- end }}
{{- define "keycloak.auth.generated-password" }}{{- randAlphaNum 16 }}{{- end }}
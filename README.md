# Using the Helm chart
The `huishoudboekje` folder is a Helm chart containing sub charts for the different parts of the application.

# Supported platforms
The Helm charts currently support the following Kubernetes environments:
* Standard Kubernetes up to version v1.24.
* Red Hat Openshift Container Platform 3 (ocp3).
* Red Hat Openshift Container Platform 4 (ocp4).

# Database
Huishoudboekje currently only supports PostgreSQL. This can be installed with Huishoudboekje as part of this Helm chart,
or an external PostgreSQL server can be used. This choice applies to all sub charts.

## External DB
The following services require a database and connection settings:
* alarmenservice
* grootboekservice
* huishoudboekjeservice
* logservice
* keycloak (optional, off by default)
* organisatieservice
* postadressenservice
* signalenservice

The accounts need full rights on their respective databases (`GRANT ALL PRIVILEGES`). This may be one account for all
services, but it is also possible to define a different server, port, database name, user and password for each service.

See also [default global values](values.yaml).

# Versioning
The parent Helm chart and all sub charts make use of [SemVer 2.0.0](https://semver.org/). This is applied to the charts as
follows:
* Breaking changes in sub charts will result in a major version increase in the respective sub chart.
* Backwards compatible feature changes will result in minor version increase.
* Backwards compatible big fixes and minor non-functional changes such as a small increase in resource limits will cause
  a patch version increase.
* The version number of the parent Helm chart will be increased along with the version number of all sub charts.

# Limitations
Currently, due to limitations of Helm, it is not possible to reference another a sub chart from another sub chart.
This results in the names of different sub charts being hardcoded in different places:
* In the [deployment manifest of the backend](charts/backend/templates/deployment.yaml).
* In the [deployment manifest of backendburgers](charts/backendburgers/templates/deployment.yaml).
* In the [template-file for the database connection URLs](templates/_db-urls.tpl).
* Every part of the application which uses the JWT secret has the name of the secret hardcoded.
* Every service has a job to test the DB connection which uses a hardcoded reference to a configmap.

If any names other than the default ones are used, bear in mind this will break the templating in the above locations.

# Known Issues
* The Keycloak install seems to be broken.

# TODO
* Find out whether platform compatibility can be solved using [capabilities](https://helm.sh/docs/chart_template_guide/builtin_objects/) instead of a global platform value. Or: [Bitnami Common Librarty Chart](https://github.com/bitnami/charts/tree/master/bitnami/common).
* Add Kubernetes v1.25 compatibility (at least the horizontal pod autoscalers need a new spec).
* Fix Keycloak.
